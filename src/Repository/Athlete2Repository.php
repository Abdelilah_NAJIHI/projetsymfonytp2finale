<?php

namespace App\Repository;

use App\Entity\Athlete2;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Athlete2|null find($id, $lockMode = null, $lockVersion = null)
 * @method Athlete2|null findOneBy(array $criteria, array $orderBy = null)
 * @method Athlete2[]    findAll()
 * @method Athlete2[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class Athlete2Repository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Athlete2::class);
    }

    // /**
    //  * @return Athlete2[] Returns an array of Athlete2 objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Athlete2
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
