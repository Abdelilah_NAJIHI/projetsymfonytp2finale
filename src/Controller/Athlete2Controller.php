<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


use App\Entity\Athlete2;
use App\Form\Athlete2Type;
class Athlete2Controller extends AbstractController
{
    
    public function index(): Response
    {
        return $this->render('main/index.html.twig', [
            'controller_name' => 'MainController',
        ]);
    }
    /**
     *  @Route("/article/add", name="add")
     * @param Request $request
     * @return Response
     */

    public function add(Request $request): Response
    {

        $athlete2 = new Athlete2();
        $form = $this->createForm(Athlete2Type::class, $athlete2);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();

            $em->persist($athlete2);
            $em->flush();

            return $this->redirectToRoute('add');
        }
     
    

      return $this->render('article/add.html.twig', [
          "form" => $form->createView()
      ]);
    }

    /**
     * @Route("article/{id}/edit", name="athlete2_edit")
     * @return Response
     */
    public function edit(Athlete2 $athlete2, Request $request): Response{

        $form = $this->createForm(Athlete2Type::class, $athlete2);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $em = $this->getDoctrine()->getManager();
            $em->flush();

            return $this->redirectToRoute('home');
        }
        return $this->render("article/edit.html.twig", [
            "form" => $form->createView()
        ]);

    }

    /**
     * @Route("article/{id}/delete", name="delete")
     * @param Athlete2 $athlete2
     * 
     */

    public function delete(Athlete2 $athlete2): Response{
        $em = $this->getDoctrine()->getManager();

        $em->remove($athlete2);
        $em->flush();

        return $this->redirectToRoute('home');
    }
    
}
