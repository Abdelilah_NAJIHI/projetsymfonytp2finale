<?php
   

namespace App\Controller;
use App\Entity\Athlete2;
use App\Repository\Athlete2Repository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    
    /**
     * @Route("/", name="home")
     * @param Athlete2Repository $athlete2Repository
     * return Response
     */
    public function home(Athlete2Repository $athlete2Repository)
    {

        $athlete2 = $this->getDoctrine()->getRepository(Athlete2 :: class)->findAll();


        
        
        return $this->render('home/index.html.twig', [
            "athlete2" => $athlete2
        ]);
    }

    
  
}
/**$em = $this->getDoctrine()->getManager();
        $athlete2 = new Athlete2();
       
    
        $athlete2->setNom('Nom');
        $athlete2->setPrenom('Prenom');
        $athlete2->setGender('Gender');
        $athlete2->setPays('Pays');
        
        $em->persist($athlete2);
        $em->flush();*/