<?php

namespace App\Form;

use App\Entity\Athlete2;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class Athlete2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom', TextType::class, [
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add('prenom',  TextType::class, [
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add('gender',  TextType::class, [
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add('pays',  TextType::class, [
                "attr" => [
                    "class" => "form-control"
                ]
            ])
            ->add('createdAt', DateType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Athlete2::class,
        ]);
    }
}
