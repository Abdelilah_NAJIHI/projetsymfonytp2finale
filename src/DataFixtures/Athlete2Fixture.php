<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Entity\Athlete2;
class Athlete2Fixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        
        
        $csv = fopen('./data/athletisme.csv', 'r'); 
        $i = 0;
        while(!feof($csv)){
            $line = fgetcsv($csv, 0, ",");
            if( $i > 0 ){
                $athlete = new Athlete2();
               
                $athlete->setPrenom(iconv("ISO-8859-1//IGNORE","UTF-8", $line[3]));
                $athlete->setNom(iconv("ISO-8859-1//IGNORE","UTF-8", $line[4]));
                $athlete->setGender ($line[5]);
                $athlete->setCreatedAt ($line[6]);
                $athlete->setPays ($line[7]);
                $manager->persist($athlete);
                if( $i % 100 == 0){
                    $manager->flush();
                    break; 
                }
            }
           
            $i = $i + 1;

        }
        $manager->flush();
    }
}
